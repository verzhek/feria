-
Tenemos que desarrollar una web simulando una FERIA que va a controlar los stands que hay, los
visitantes a la feria, y va a registrar las visitas de cada usuario en los stands. Tendremos un módulo de
administración, una sencilla api para ver (o registrar) visitas, y una pequeña app responsive para registrar las
visitas a los stand
-
Las vistas deben ser Blade de Laravel. Debemos separar las rutas en grupos.
El diseño se deja a vuestra elección, pero no debe ser el que viene por defecto en Laravel.
Debes crear seeders con factorías. Añade 100 usuarios, 25 stands, y simula que cada usuario visite al
menos 10 stands.
En todas las inserciones y modificaciones se harán las validaciones pertinentes.
Habrá que incluir paginadores cada vez que se muestren varios datos.
Cada acción de base de datos grabará un mensaje en el registro de logs.
El código deberá estar en Github (empieza a desarrollar desde un repositorio nuevo) y debe estar
desplegado en Heroku.

Para este proyecto se va a emplear:
Laravel 8.0 con php artisan 
Composer 2.0.11
Apache2 para servidor
PHP7.4 con todos los modulos instalados y puestos a funcionar
Mysql-server para base de datos
Mysql Workbench para trabajo directo con la base de datos.
